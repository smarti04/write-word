from docx import Document
from docx.shared import Inches
from docx.shared import Pt

class monDocument():
    def __init__(self, name):
        self.doc = Document()
        self.name = name
        style = self.doc.styles['Normal']
        font = style.font
        font.name = 'Arial'
        font.size = Pt(10)
    def addHeading(self, title ='The role of dolphins',level = 1):
        heading = self.doc.add_heading(title, level=level)
    def addParagraph(self, text = 'Lorem ipsum dolor sit amet.'):
        paragraph = self.doc.add_paragraph(text)
        paragraph.style = self.doc.styles['Normal']
    def addPicture(self, picPath = 'LaRochelle.PNG'):
        picture = self.doc.add_picture(picPath, width=Inches(5.0))
    def addTable(self, df):
        t = self.doc.add_table(df.shape[0]+1, df.shape[1])
        t.style = 'Table Grid'
        # add the header rows.
        for j in range(df.shape[-1]):
            t.cell(0, j).text = df.columns[j]

        # add the rest of the data frame
        for i in range(df.shape[0]):
            for j in range(df.shape[-1]):
                t.cell(i + 1, j).text = str(df.values[i, j])
    def save(self):
        self.doc.save('{}.docx'.format(self.name))
